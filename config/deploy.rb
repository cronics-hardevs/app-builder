# config valid for current version and patch releases of Capistrano
lock "~> 3.11.0"

set :application, "app_builder"
set :repo_url, "git@github.com:botmline/app-builder.git"

# Default branch is :master
# ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp
set :branch, :master

# Default deploy_to directory is /var/www/my_app_name
set :deploy_to, "/home/www/services/app-builder"

# Default value for :format is :airbrussh.
# set :format, :airbrussh

# You can configure the Airbrussh format using :format_options.
set :format_options, command_output: true, log_file: "var/log/capistrano.log", color: :auto, truncate: :auto

# Default value for :pty is false
# set :pty, true

# Default value for :linked_files is []
# append :linked_files, "config/database.yml"
set :linked_files, [".env.local"]

# Default value for linked_dirs is []
# append :linked_dirs, "log", "tmp/pids", "tmp/cache", "tmp/sockets", "public/system"
append :linked_dirs, "vendor"

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for local_user is ENV['USER']
# set :local_user, -> { `git config user.name`.chomp }

# Default value for keep_releases is 5
set :keep_releases, 3

# Uncomment the following to require manually verifying the host key before first deploy.
# set :ssh_options, verify_host_key: :secure

# Composer flags, --quiet is dropped from defaults, --no-progress is added
set :composer_install_flags, "--no-dev --prefer-dist --no-interaction --optimize-autoloader --no-progress"

# misc
set :php_fpm_service_name, "php-fpm.builders"

# chmod
set :permission_method, :chmod
set :file_permissions_chmod_mode, "0775"
set :file_permissions_roles, [:app]

# Run composer/console on app servers only
set :composer_roles, [:app]
set :symfony_deploy_roles, [:app]
set :symfony_roles, [:app]

# bin/console run options
#set :symfony_console_flags, "--no-debug --env=#{fetch :symfony_env}"
set :default_env, {
 :app_env => 'prod',
}

# Map composer binary
SSHKit.config.command_map[:composer] = "#{shared_path.join('composer.phar')}"
namespace :deploy do
  after :updating, "composer:install_executable"
  after :finishing, 'service:php_fpm:reload'
  after :finishing, 'service:nginx:reload'
end

namespace :service do
  namespace :nginx do
    desc "Reload Nginx config"
    task :reload do
      on release_roles(:web) do
        execute :sudo, "service", fetch(:nginx_service_name, "nginx"), "reload"
      end
    end
  end

  namespace :php_fpm do
    desc "Reload PHP-FPM config"
    task :reload do
      on release_roles(:app) do
        execute :sudo, "service", fetch(:php_fpm_service_name, "php-fpm"), "reload"
      end
    end
  end
end

before "deploy:updated", "symfony:cache:clear"
